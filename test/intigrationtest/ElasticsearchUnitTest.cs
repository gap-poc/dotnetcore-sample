using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using HostApi.Model;
using Nest;
using System.Diagnostics;

namespace intigrationtest
{
    
    public class ElasticsearchUnitTest
    {
        private const string ConnectionString = "http://localhost:9200/";
        private ElasticClient client;
       
       public ElasticsearchUnitTest()
       {
           var node = new Uri(ConnectionString);
           var setting = new ConnectionSettings(node);

           client = new ElasticClient(setting);
       }

       [Fact]
       [Trait("Category","es")]
       public void Elasticsearch_Indexing_Test()
       {
            var obj = new Product{
                Name = "Test",
                Category = "Test Category"
            };

            var response = client.Index(obj, idx=> idx.Index("myProductIndex"));

            Trace.Write(response);
       }
 

    }
}